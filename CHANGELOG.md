v0.0.1

## CHANGELOG

### v0.0.1

#### Additions

* Added UI base lib - [944f335](https://github.com/adonaac/blade/commit/944f3358ed9986636d6f8e06e8136d7cb3c10512) (2015-07-21 04:49:54)
* Added base library structure - [4ab30d6](https://github.com/adonaac/blade/commit/4ab30d670e3b056cf215f54f9ab011a301c7884f) (2015-07-23 13:52:53)
* Added View class - [4ab30d6](https://github.com/adonaac/blade/commit/4ab30d670e3b056cf215f54f9ab011a301c7884f) (2015-07-23 13:52:53)
* Added Stack class - [4ab30d6](https://github.com/adonaac/blade/commit/4ab30d670e3b056cf215f54f9ab011a301c7884f) (2015-07-23 13:52:53)
* Added Flow class - [4ab30d6](https://github.com/adonaac/blade/commit/4ab30d670e3b056cf215f54f9ab011a301c7884f) (2015-07-23 13:52:53)
* Added IconButton element - [4ab30d6](https://github.com/adonaac/blade/commit/4ab30d670e3b056cf215f54f9ab011a301c7884f) (2015-07-23 13:52:53)
* Added 'name' and index based object retrieval from the UI tree - [4ab30d6](https://github.com/adonaac/blade/commit/4ab30d670e3b056cf215f54f9ab011a301c7884f) (2015-07-23 13:52:53)
* Added UI element automatic placement under Stacks or Flows - [b72025a](https://github.com/adonaac/blade/commit/b72025ae6dfea0ddecfeeacb2ca9aa30fa67cd52) (2015-07-24 12:47:02)
* Added Stack and Flow building logic - [b72025a](https://github.com/adonaac/blade/commit/b72025ae6dfea0ddecfeeacb2ca9aa30fa67cd52) (2015-07-24 12:47:02)
* Added Stack and Flow mixing logic - [b72025a](https://github.com/adonaac/blade/commit/b72025ae6dfea0ddecfeeacb2ca9aa30fa67cd52) (2015-07-24 12:47:02)

#### Fixes

* Fixed issues with Stack, Flow stacking - [e9f4e10](https://github.com/adonaac/blade/commit/e9f4e10a1e2dd95f47c63d1079c46b3032f46220) (2015-07-25 01:34:01)
* Fixed all Stack and Flow mixing issues - [21cdc2f](https://github.com/adonaac/blade/commit/21cdc2fb28d7ea33d243ed3272155b1b7367c5c6) (2015-07-25 02:17:43)

#### Updates

* Finished high level Stack and Flow functionality - [21cdc2f](https://github.com/adonaac/blade/commit/21cdc2fb28d7ea33d243ed3272155b1b7367c5c6) (2015-07-25 02:17:43)
* Changed name to footsies - [96cbcd9](https://github.com/adonaac/blade/commit/96cbcd9544ea63e6e4a053fe587728b0d2a9aba5) (2015-07-25 04:15:00)

---

